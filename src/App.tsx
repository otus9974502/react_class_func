import React from "react";
import "./App.css";
import { UrlInput } from "./Components/UrlInput/UrlInput";
import { UrlInputFunc } from "./Components/UrlInput/UrlInputFunc";

function App() {
  return (
    <>
      На классах:
      <hr />
      <UrlInput />
      <br />
      На функах:
      <hr />
      <UrlInputFunc />
    </>
  );
}

export default App;
