import { Component, ReactNode } from "react";
import CSS from 'csstype';

interface ErrorDataProps {
    code: string,
    message: string,
    stack: string,
    hasError: boolean
}

const codeStyle: CSS.Properties = {
    color: 'red',
    fontSize: '10pt'
}

const messageStyle: CSS.Properties = {
    color: 'red',
    fontSize: '12pt'
}

const stackStyle: CSS.Properties = {
    color: 'brown',
    fontSize: '10pt'
}

export class ErrorData extends Component<ErrorDataProps> {

    render(): ReactNode {
        if(this.props.hasError)
        {
            return <div style={{padding: '10px', margin: '10px', border: "2px dashed red"}}>
                <div style={codeStyle}>{this.props.code}</div>
                <div style={messageStyle}>{this.props.message}</div>
                <div style={stackStyle}>{this.props.stack}</div>
            </div>
        } else {
            return <></>;
        }
    }
}