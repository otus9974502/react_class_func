import { Component, ReactNode } from "react";
import CSS from 'csstype';

interface ResponseDataProps {
    text: string;
}

export class ResponseData extends Component<ResponseDataProps> {
    render(): ReactNode {
        return <div>{this.props.text}</div>
    }
}