import { Component, ReactNode } from "react";
import axios from "axios";
import { ResponseData } from "../ResponseData/ResponseData";
import { ErrorData } from "../ErrorData/ErrorData";


interface UrlState {
    inputValue: string,
    responseData: string,
    responseHasError: boolean,
    code: string,
    message: string,
    stack: string
}

export class UrlInput extends Component<{}, UrlState> {
    constructor(props: any)
    {
        super(props);

        this.state = {
            inputValue: "",
            responseData: "",
            responseHasError: false,
            code: "",
            message: "",
            stack: ""
        }
    }

     change = (e: any) => {
        this.setState({
            responseHasError: false, 
            code: "",
            message: "",
            stack: "",
            inputValue: e.target.value
        });
     }

    click = () => {
        axios.get(this.state.inputValue, {
            validateStatus: status => {
                this.setState({ responseHasError: status != 200 });
                return status == 200;
            }
        })
        .then(res => {
            this.setState({ 
                responseData: JSON.stringify(res.data), 
            });
        })
        .catch(error => {
            this.setState({ 
                responseHasError: true, 
                responseData: "",
                code: error.code,
                message: error.message,
                stack: error.stack
            });
        });
    }

    render(): ReactNode {
        return <>
            <div>
                Url: <input 
                onChange={this.change}
                placeholder="Input url"
                type="url" id="url"
                />
                <button onClick={this.click}>Отправить</button>
            </div>
            <div>
                <ResponseData text={this.state.responseData}/>
                <ErrorData code={this.state.code} message={this.state.message} stack={this.state.stack} hasError={this.state.responseHasError}/>
            </div>
        </>;
    }
}