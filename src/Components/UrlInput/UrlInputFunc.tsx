import axios from "axios";
import { ErrorData } from "../ErrorData/ErrorData";
import { ResponseData } from "../ResponseData/ResponseData";
import { useState } from "react";

export function UrlInputFunc(props: any) {
  const [inputValue, setInputValue] = useState("");
  const [responseData, setResponseData] = useState("");
  const [responseHasError, setResponseHasError] = useState(false);
  const [code, setCode] = useState("");
  const [message, setMessage] = useState("");
  const [stack, setStack] = useState("");

  const handleChange = (event: any) => {
    setInputValue(event.target.value);
  };

  const handleClick = (event: any) =>
    axios
      .get(inputValue, {
        validateStatus: (status: any) => {
          return status == 200;
        },
      })
      .then((res: any) => {
        setResponseData(JSON.stringify(res.data));
      })
      .catch((error: any) => {
        setResponseHasError(true);
        setResponseData("");
        setCode(error.code);
        setMessage(error.message);
        setStack(error.stack);
      });

  return (
    <>
      <div>
        Url:{" "}
        <input
          onChange={handleChange}
          placeholder="Input url"
          type="url"
          id="url"
        />
        <button onClick={handleClick}>Отправить</button>
      </div>
      <div>
        <ResponseData text={responseData} />
        <ErrorData
          code={code}
          message={message}
          stack={stack}
          hasError={responseHasError}
        />
      </div>
    </>
  );
}
